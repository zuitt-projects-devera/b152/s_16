function displayMsgToSelf() {
console.log('Lorem, ipsum dolor sit amet consectetur, adipisicing.');
}

for (var i = 0; i < 10 ; i++) {

displayMsgToSelf();
}

/* [Try-Catch-Finally Statement] 
	- "try catch" statements are commonly used for error handling
	- there are instances when the application returns an error/warning that is not 
	necessarily and error in the context of our code
	- these errors are a result of an attempt of the programming language to help
	developer in creating effiient code
	- they are used to specify a respinse whenever an exception/error is received
	- it is also useful for debugging code because of the "error" object that can be caught
	- In programming languages, an "error" object is used to provide detailed information
	about an error and which also provides access to functions that can be used to 
	handle/resolve errors to create "exceptions" within our code
	- The "finally" block is used to specify a response/action that is used to 
	handle/resolve errors



*/
/*
function showIntensityAlert (windSpeed) {
	try {
		// attempt to execute a code
		alert (determineTyphoonIntensity(windSpeed));
	} catch(e) {
		// error/err/e commonly used variable names used by the developers for storing error
		// in this case the error is an unkown function "alert" which does not exist in Javascript
		// the 'alert' function is used similarly to a prompt to alert the user
		// 'error.message'
		console.log(typeof e);
		console.log(e.message);
	}

	finally { 
		alert("Intensity updates will show new alert.");

	}

}*/
/*
const num = 100; x = 'a';

console.log(num);
try { 
	console.log(num/x);
	console.log(a);
}
catch (error) {
	console.log('An error caught.');
	console.log('Error message: ' + error);
}
finally { 
	alert('Finally will execute.');
}*/

function gradeEvaluator(input) {

let user_input = document.getElementById('userinput').value;

input = user_input;
console.log(input);	

try {
	if(input == "")  throw "is empty";
    if(isNaN(input)) throw "is not a number";
   
    if(input > 100)   throw "is too high";

	if (input >= 90) {
		document.getElementById("output").innerHTML = "Your Grade is A.";
	}
	else if (input >= 80){
		document.getElementById("output").innerHTML = "Your Grade is B.";
	}
	else if (input >= 71){
		document.getElementById("output").innerHTML = "Your Grade is C.";
	}
	else if (input <= 70){
		document.getElementById("output").innerHTML = "Your Grade is F.";
	}
	else {
		document.getElementById("output").innerHTML = "Your Grade is F.";
	}
}
catch(e) {
	document.getElementById("output").innerHTML = "Input " + e;
}

finally {
	document.getElementById("userinput").value = "";
}

}


/* THIS LESSON IS LOOOOOPPPIIIIING */


//WHILE LOOP 
// allows us to repeat an action or an instruction as long as the condition is true.

/*let count = 10;

while (count !== 0) {
	displayMsgToSelf();
	count--;
}


function countTo15() {
	let initialCount = 1;
	while (initialCount !== 16) {
		console.log("COUNT: " + initialCount);
		 initialCount++;
	}
}*/


/* DO WHILE LOOP*/
 /*Do while loop is similar to while loop. However, Do While loop will run the code block 
 at least once befure it checks the condition 

 /* With While loop, we first check the condition and then run the code block/statements* */

 function doWhile() {
let doWhileCounter = 20;

do {
	console.log(doWhileCounter)
	--doWhileCounter;
} while (doWhileCounter <= 0);

}



/* For Loop 
	


*/


/* display characters of a string using for loop*/

function forLoopString() {
let name = "Andrae";
for (let index = 0; index < 6; index ++) {
	console.log(name[index]);
}
}


/*Using for loop in an array*/
let fruit = ["mango", "apple", "orange", "banana", "strawberry", "kiwi"]

//elements - each value inside the array
console.log(fruit);

//total count of elements in an array
console.log(fruit.length);
//Access each element in an array
console.log(fruit[0]);
console.log(fruit[5]);
console.log(fruit[4]);

//get the last element in an array if we don't have the total number of elements.
/*console.log(fruit[fruit.length - 1]);*/

//if we want to assign new value to an element.
fruit[6] ="grapes";
console.log(fruit);

for (let i = 0; i < fruit.length; i++) {
	console.log(fruit[i]);
}













/* Creating an Array from an object properties



let obj = {
	name: 'blue',
	age: 24,
	address: 'Philippines'
}

let arrayofobj = new Array();
arrayofobj[0] = obj.name;
arrayofobj[1] = obj.age;
arrayofobj[2] = obj.address;


console.log(arrayofobj);
*/

let cars = [

{
	brand: "Toyota",
	type: "Sedan" 
},
{
	brand: "Rolls Royce",
	type: "Luxury Sedan"
},
{
	brand: "Mazda",
	type: "Hatchback"
}
]
console.log(cars[0]);
/*Creating array of objects*/
function arrayOfObs () {
	for (let i = 0; i < cars.length; i++) {
		console.log(cars[i]);
	}
}

function miniActivity(){
let myName = document.getElementById("userinput").value;
let result = myName.toLowerCase();
for (let char = 0; char < myName.length; char++) {
	if (result[char] == result[char].match(/[^aeiou]/gi)) {
		console.log(result[char]);
		}
	
	else {
		console.log(3);

}
}
}


function miniActivity2 () {
	let str = "aluuuD";
	for (let i = 0; i < str.length; i++) {
		if (str[i].toLowerCase() == "a") {
			console.log("Continue to next iteration.");
			continue;
		} else if (str[i].toLowerCase() == "d") {
			break;
		}
		else {
			console.log("NO LETTERS A AND D");
		}
	}
}


/* NESTED FOR LOOP */
for (let x = 0; x <= 10; x++){
	console.log();
		
	for(let y = 0; y <= 10; y++) {
		
	}
}